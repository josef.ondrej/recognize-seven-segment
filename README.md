Glucometer Display OCR Library
=================================

Library for character recognition of data displayed on glucometers. Particulary the FreeStyle Libre glucometer. 
This was created in under 48h CEE Smart Health Hackathon 2019 https://www.ceehacks.com/shhprague2019/.

The main API for communication with the other components is in `recognize_seven_segment/hackathon_api/recognize_number.py`.